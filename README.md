# Programación Declarativa Orientaciones
```plantuml

@startmindmap
title Programacion declarativa
center footer Herrera Mendoza Juan Antonio
<style>
mindmapDiagram {
  .nivel1 {
    BackgroundColor lightgreen
  }
  .nivel2 {
    BackgroundColor #64EFCB
  }
  .nivel3 {
    BackgroundColor #64E4EF
  }
  .nivel4 {
    BackgroundColor #79BCF9
  }
  .nivel5 {
    BackgroundColor #797FF9
  }
  .nivel6 {
    BackgroundColor #B379F9
  }
  .nivel7 {
    BackgroundColor #F279F9
  }
  .nivel8 {
    BackgroundColor #F9799D
  }
  .green {
    BackgroundColor lightgreen
  }
  .red{
      BackgroundColor #DE7171
  }
}
</style>

* Programacion Declarativa <<red>>
** Programacion <<green>>
***_ Dividido en
**** Parte creativa <<nivel2>>
***** Diseño <<nivel3>>
**** Parte burocratica <<nivel2>>
***** Implementacion <<nivel3>>
**_ Es un 
*** Paradigma de programacion <<green>>
****_ Enfocado en
***** Abandonar secuencias de orden <<nivel2>>
******_ En el
******* Control de la memoria <<nivel3>>
**_ Surge para 
*** Solucion de problemas <<green>>
****_ De 
***** Programacion imperativa <<nivel2>>
******_ Como lo son 
******* Muchos detalles de calculos <<nivel3>>
******* Modificacion del estado de la memoria por pasos <<nivel3>>
******* Asignacion en todo momento <<nivel3>>
*** Especificacion en nivel mas alto <<green>>
****_ Mediante 
***** Recursos expresivos <<nivel2>>
******_ Teniendo 
******* Ventajas <<nivel3>>
******** Programas <<nivel4>>
********* Mas cortos <<nivel5>>
*********_ Faciles de  
********** Realizar <<nivel6>>
********** Depurar  <<nivel6>>
******* Desventajas <<nivel3>>
******** Abstracto <<nivel4>>
*********_ No es evidente
********** Programar <<nivel6>>
********_ Sin el nivel correcto de
********* Iteraccion con la maquina <<nivel6>>
**********_ El programa puede
*********** Alejarse de la implementacion correcta <<nivel7>>
*********** Ser ineficiente  <<nivel7>>

left side
** Variantes <<green>>
*** Programacion Funcional <<nivel2>>
****_ Utiliza 
***** Lenguaje Matematico  <<nivel3>>
******_ Con
******* Reglas de simplificacion <<nivel4>>
********_ Para  
********* Describir funciones <<nivel5>>
**********_ Mediante 
*********** Ecuaciones <<nivel6>>
**** Datos de entrada <<nivel3>>
*****_ Son
****** Argumentos <<nivel4>>
**** Datos de salida <<nivel3>>
*****_ Son
****** Resultados de funciones <<nivel4>>
**** Ventajas <<nivel3>>
***** Funciones de orden superior <<nivel4>>
****** Entrada de un programa <<nivel5>>
*******_ Puede ser
******** Otro programa <<nivel6>>
***** Funciones sobre funciones <<nivel4>>
******_ En lugar de
******* Datos primitivos <<nivel5>>
***** Evaluacion perezosa <<nivel4>>
******_ Consiste
******* Evaluacion <<nivel5>>
********_ Unicamente de
********* Ecuaciones Necesarias <<nivel6>>
**********_ Para el
*********** Programa <<nivel7>>
***** Tipos de datos infinitos  <<nivel4>>

*** Programacion Logica <<nivel2>>
****_ Uso de
***** Predicados Logicos <<nivel3>>
***** Axiomas <<nivel3>>
***** Reglas de inferencia <<nivel3>>
******_ De
******* Primer Orden <<nivel4>>
**** Relaciones <<nivel3>>
*****_ Entre
****** Objetos definidos <<nivel4>>
**** Orden no establecido <<nivel3>>
*****_ Entre
****** Datos de entrada <<nivel4>>
****** Datos de salida <<nivel4>>
**** Programas  <<nivel3>>
*****_ Basados en
****** Hechos <<nivel4>>
****** Reglas <<nivel4>>
**** El interprete <<nivel3>>
*****_ A partir de
****** Conjunto de predicados <<nivel4>>
*******_ Prueba la
******** Veracidad en ellos <<nivel5>>
**** Relacion  <<nivel3>>
*****_ Con
****** Programacion Orientada a IA <<nivel4>>


@endmindmap
```


Referencia Bibliografica:

[Programacion Declarativa 1998](https://canal.uned.es/video/5a6f2c66b1111f54718b4911)

[Programación Declarativa Orientaciones y pautas para el estudio 1999](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b)

[Programación Declarativa. Orientaciones 2001](https://canal.uned.es/video/5a6f2c42b1111f54718b4757)

# Lenguaje de Programación Funcional

```plantuml

@startmindmap
title Programacion funcional
center footer Herrera Mendoza Juan Antonio
<style>
mindmapDiagram {
  .purple {
    BackgroundColor #C944A1
  }
  .blue{
      BackgroundColor #54F1CD
  }
  .nivel2 {
    BackgroundColor #54F19A
  }
  .nivel3 {
    BackgroundColor #6AF154
  }
  .nivel4 {
    BackgroundColor #81F154
  }
  .nivel5 {
    BackgroundColor #B0F154
  }
  .nivel6 {
    BackgroundColor #E4F154
  }
  .nivel7 {
    BackgroundColor #F1DC54
  }
  .nivel8 {
    BackgroundColor #F1B354
  }
  .nivel9 {
    BackgroundColor #F19154
  }
  .nivel10 {
    BackgroundColor F16B54
  }
  .nivel11{
      BackgroundColor #DE7171
  }
}
</style>

* Programacion Funcional <<purple>>
** Paradigmas de programacion <<blue>>
***_ Son
**** Modelos de computacion <<nivel2>>
*****_ Los cuales mediante
****** Lenguajes de programacion <<nivel3>>
*******_ Dan
******** Semantica <<nivel4>>
*********_ A un 
********** Programa <<nivel5>>
** Lenguajes de programacion <<blue>>
***_ Surguieron siguiendo
**** Modelo de von Neumann <<nivel2>>
*****_ Llamada
******  Programacion imperativa <<nivel3>>
*******_ Consiste en
******** Programas almacenados <<nivel4>>
*********_ En la 
********** Misma maquina <<nivel5>>
***********_ Previo a ser
************ Ejecutados <<nivel6>>
********* Instrucciones Secuenciales <<nivel5>>
********* Modificacion posterior <<nivel5>>
*********_ Mediante
********** Estado de computo <<nivel5>>
********** Variables <<nivel5>>
** Programacion Logica <<blue>>
***_ Basada en
**** Logica simoblica <<nivel2>>
****_ Usa
***** Sentencias <<nivel2>>
******_ Para definir
******* Verdades <<nivel3>>
***** Inferencia <<nivel2>>
******_ Mediante
******* Datos de entrada <<nivel3>>
********_ Para
********* Resolver problemas <<nivel4>>
** Programacion funcional <<blue>>
*** Bases <<nivel2>>
**** Lambda calculo <<nivel3>>
***** Surgio <<nivel4>>
******_ En 
******* 1930 <<nivel5>>
*****_ Creada por
****** Alonzo Church <<nivel5>>
****** Stephen Kleene <<nivel5>>
***** Sistema <<nivel4>>
******_ Para
******* Aplicacion de funciones <<nivel5>>
******* Uso de recursividad <<nivel5>>
**** Logica combinatoria <<nivel3>>
*****_ Basada en
****** Lambda Calculo <<nivel4>>
*****_ Creada por
****** Haskell Curry <<nivel4>>
*** Programas <<nivel2>>
****_ Creados mediante
***** Funciones matematicas <<nivel3>>
******_ Reciben
******* Parametros de entrada <<nivel4>>
********_ Generan
********* Datos de salida <<nivel5>>
*** Caracteristicas <<nivel2>>
****_ Inexistencia de
***** Estado de computo <<nivel3>>
***** Asignacion <<nivel3>>
***** Bucles <<nivel3>>
******_ Resueltos mediante
******* Recursividad <<nivel4>>
****_ Existencia de
***** Varibales <<nivel3>>
******_ Solo como
******* Referencia <<nivel4>>
********_ De
********* Parametros de entrada <<nivel5>>
***** Constantes <<nivel3>>
******_ Son
******* Funciones  <<nivel4>>
********_ Que siempre dan
********* Mismo resultado <<nivel5>>
**** Programas dependientes <<nivel3>>
*****_ de
****** Parametros de entrada <<nivel4>>
*******_ Se conoce como
******** Transaparencia refrencial <<nivel5>>
**** Resultados independientes <<nivel3>>
*****_ Del
****** Orden de calculos <<nivel4>>
**** Entorno multiprocesador <<nivel3>>
*****_ Ejecucion de
****** Mismos algoritmos <<nivel4>>
*******_ Con
******** Diferentes datos <<nivel5>>
*********_ Devolveran siempre
********** Mismos resultados <<nivel6>>
**** Funciones de orden superior <<nivel3>>
*****_ Consiste en 
****** Funciones <<nivel4>>
*******_ Utilizadas como
******** Parametros <<nivel5>>
*********_ De
********** Otras funciones <<nivel6>>
**** Currificacion <<nivel3>>
***** Funcion con diferentes <<nivel4>>
****** Parametros de entrada <<nivel5>>
*******_ Devuelve diferentes
******** Salidas o funciones <<nivel6>>
**** Aplicacion Parcial <<nivel3>>
*****_ Aplicar una
****** Funcion <<nivel4>>
*******_ A solo un
******** Conjunto de parametros <<nivel5>>
**** Evaluacion de funciones <<nivel3>>
***** Evaluacion estricta  <<nivel4>>
****** Evaluacion  <<nivel5>>
*******_ de
******** Expresiones internas <<nivel6>>
*********_ Anterior a 
********** Expresiones externas <<nivel7>>
***** Evaluacion no estricta <<nivel4>>
******_ Ocurre de
******* Fuera hacia adentro <<nivel5>>
********_ No necesita
********* Conocer el valor <<nivel6>>
**********_ De todos los
*********** Parametros <<nivel7>>
************_ Para
************* Devolver un resultado <<nivel8>>
****** Memoizacion <<nivel5>>
*******_ Consiste en
******** Almacenar el valor <<nivel6>>
*********_ De una
********** Expresion <<nivel7>>
***********_ Que ya
************ Se evaluo <<nivel8>>
******_ LLamada tambien
******* Evaluacion perezosa <<nivel4>>
**** Lenguajes de programacion funcional <<nivel3>>
*****_ Busqueda de
****** Eficiencia <<nivel4>>
*******_ En base a
******** Rapidez de escriura de codigo <<nivel5>>

@endmindmap
```

Referencia Bibliografica:

[Lenguaje de Programación Funcional (2015)](https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708)